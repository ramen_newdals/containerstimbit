from dataclasses import dataclass
import matplotlib.pyplot as plt
import numpy as np
import pyvista as pv


pv.global_theme.font.color = 'grey'

#https://docs.pyvista.org/examples/03-widgets/multi-slider-widget.html


#create an object class to be able to easily update attributes
class Meshes:
    #the mesh object can now be referenced using m.header e.g. m.header.set_active_scalars("lnh")
    def __init__(self, name):
        self.header= name

    #replace scalar name with whatever the scalars have been names in paraview, must be a string
    #use the command print(mesh.header.cell_data) or print(mesh.header.point_data) to print scalar names
    vnr_vmag_scalar="VNR_Vmag"
    lnh_scalar='lnh'
    vnr_mask_percent=50,
    lnh_threshold= 0.2,
    pos_lnh=[]
    neg_lnh=[]


    #this function collects all the slider inputs that need to interact with the class object
    def __call__(self, arg, value):
        if (arg == 'lnh_threshold'):
            self.lnh_threshold = value
        if (arg == 'vnr_mask_percent'):
            self.vnr_mask_percent = value
        self.vnr_update()
    
    def vnr_update(self):
        # any time the vnr slider is touched, this function will be called to recalculate and plot the vnr lnh meshes
        calculate_vnr_meshes(m)
        return


def calculate_vnr_meshes(m):
    #use this when referencing mesh by name for pyvista functions
    mesh_h=m.header

    #need to always specity which scalar in the mesh is being used for current operation
    mesh_h.set_active_scalars(m.lnh_scalar)
    lnh_thresh=m.lnh_threshold/100
    neg_lnh_thresh= float (0) - float(lnh_thresh)
    print("lnh " + str(lnh_thresh) + " " + str(neg_lnh_thresh) )

    pos_lnh= mesh_h.threshold(lnh_thresh)
    neg_lnh= mesh_h.threshold(neg_lnh_thresh,invert=True)

    mesh_h.set_active_scalars(m.vnr_vmag_scalar) 
    #calculate positive and negative lnh when vnr is HIGH, these will be opaque red and blue
    pos_lnh_high_vnr= pos_lnh.threshold_percent(m.vnr_mask_percent) 
    neg_lnh_high_vnr= neg_lnh.threshold_percent(m.vnr_mask_percent)

    #calculate positive and negative lnh when vnr is LOW, these will be transparent red and blue
    pos_lnh_low_vnr=pos_lnh.threshold_percent(m.vnr_mask_percent,invert=1) #to flip threshold add argument invert=1
    neg_lnh_low_vnr=neg_lnh.threshold_percent(m.vnr_mask_percent,invert=1) #to flip threshold add argument invert=1

    #plot the 4 meshes from above
    p.add_mesh(pos_lnh_high_vnr, name="lnh_pos", color='red', opacity=1)
    p.add_mesh(neg_lnh_high_vnr, name="lnh_neg", color='blue',opacity=1)

    p.add_mesh(pos_lnh_low_vnr, name="lnh_pos_low_vnr", color='tomato', opacity=0.2)
    p.add_mesh(neg_lnh_low_vnr, name="lnh_neg_low_vnr", color='cornflowerblue',opacity=0.2)

    #pass all calculated meshes back into the mesh object so they can be plotted
    m.pos_lnh= pos_lnh
    m.neg_lnh= neg_lnh


#lnh with thresholding, without, unmasked data, the mask itself
data= pv.read('vnr.vtk')
streamlines= pv.read("streamlines.vtk")
#m is short for main object
m = Meshes(data)

p = pv.Plotter()
p.set_background('white')

p.add_mesh(streamlines,scalars='MetaImage',name="streamline")
#pass main object into the function to add all the new calculated meshes


slider_vnr_limit = p.add_slider_widget(
    callback=lambda value: m('vnr_mask_percent', int(value)),
    rng=[1, 99],
    value=50,
    title="vnr_percent",
    pointa=(0.01, 0.65),
    pointb=(0.21, 0.65),
    style='classic'
)


slider_lnh = p.add_slider_widget(
    callback=lambda value: m('lnh_threshold', int(value)),
    rng=[0,99],
    value=50,
    title="LNH Threshold",
    pointa=(0.01, 0.5),
    pointb=(0.21, 0.5),
    style='classic'
)


p.show()